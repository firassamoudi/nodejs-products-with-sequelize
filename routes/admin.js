const path = require('path');

const express = require('express');

const AdminController = require('../controllers/admin');

const router = express.Router();

// /admin/add-product => GET
router.get('/add-product', AdminController.getAddProduct);

router.get('/product',AdminController.getProducts);

// /admin/add-product => POST
router.post('/add-product', AdminController.postAddProduct);


// /admin/edit-product => GET
router.get('/edit-product/:productId', AdminController.getEditProduct);

router.post('/edit-product/',AdminController.postEditProduct);

router.post('/delete-product/',AdminController.postDeleteProduct);
module.exports = router;
